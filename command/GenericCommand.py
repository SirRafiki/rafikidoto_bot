
from .DefaultCommand import Command as DefaultCommand
from .Dota2Command import Command as Dota2Command


dicCommand={
"!help":DefaultCommand.help,
"!heros":Dota2Command.get_heros
}

class Command:
    def __init__(self,message,client):
        self.message = message
        msgCommand = message.content
        tmp = msgCommand.split()
        self.command = tmp[1]
        del tmp[:2]
        self.args = tmp
        self.client = client
    def check(self):
        return dicCommand[self.command](args = self.args,client = self.client,message = self.message)
