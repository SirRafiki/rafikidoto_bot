#!/usr/bin/python3
from os import environ
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(), override=True)

class settings:
    def get(conf):
        value = environ.get(conf)
        if not value:
            return Exception("Key not found")
        return value
