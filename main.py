import discord
import asyncio
from  core.settings import settings
from command.GenericCommand import Command

client = discord.Client()

@client.event
async def on_ready():
    print('Let\'s GO\n')

@client.event
async def on_message(message):
    if message.mentions:
        if message.mentions[0] == client.user:
            command = Command(message,client)
            ret = command.check()
            await client.send_message(message.channel,"{}".format(ret))

client.run(settings.get("TOKEN_DISCORD"))
